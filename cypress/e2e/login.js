const { Given, When, Then } = require("@badeball/cypress-cucumber-preprocessor");

Given("I am in the login page of the Swag Labs Web Application", () => {
  cy.visit("https://www.saucedemo.com/");
});

When("I enter {string} as username and {string} as password", (username, password) => {
  console.log(` Here: ${username}  and ${password}.`);
  cy.get("input[data-test='username']").type(username);
  cy.get("input[data-test='password']").type(password);
});

When("I click the login button", () => {
  console.log("click on login button");
  cy.get("input[data-test='login-button']").click();
});

Then("I should be taken to the product page", () => {
  console.log("Products page");
  cy.get(".inventory_container").should('be.visible');
});

Then("I should see an error message", () => {
  cy.get("h3[data-test='error']").should('be.visible');
});
