const { Given, When, Then } = require("@badeball/cypress-cucumber-preprocessor");

const inventoryList = ".inventory_item_name";
const sortFilter = "select[data-test='product_sort_container']";

Given("I have logged in to the application", () => {
    cy.visit("https://www.saucedemo.com/");
    cy.get("input[data-test='username']").type("standard_user");
    cy.get("input[data-test='password']").type("secret_sauce");
    cy.get("input[data-test='login-button']").click();
    console.log("LOgin ooggdsdd");
});


Given("I am in the product page of the Swag Labs Web Application", () => {
    cy.url().should("include", "/inventory");
});

When("the products are sorted from A-Z", () => {
    getProductNames().then((productNames) => {
        assert.isTrue(isSortedAscending(productNames));
    });
});

When("I select Name\\(Z to A) from the sort filter", () => {
    cy.get(sortFilter).select("za");
});

Then("the products should be filtered from Z to A", () => {
    getProductNames().then((productNames) => {
        assert.isTrue(isSortedDescending(productNames));
    });
});

Then("I should be taken to the product page", () => {
    cy.url().should("include", "/inventory");
});

function getProductNames() {
    return cy.get(inventoryList).then(($elements) => {
        const productNames = [];
        $elements.each((_, element) => {
            productNames.push(element.innerText);
        });
        return productNames;
    });
}

function isSortedAscending(list) {
    for (let i = 1; i < list.length; i++) {
        if (list[i - 1].localeCompare(list[i]) > 0) {
            return false;
        }
    }
    return true;
}

function isSortedDescending(list) {
    for (let i = 1; i < list.length; i++) {
        if (list[i - 1].localeCompare(list[i]) < 0) {
            return false;
        }
    }
    return true;
}
