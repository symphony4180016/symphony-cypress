Feature: User Login Functionality
    In order to use Swag Labs
    As a valid user
    I want to  login successfully

  Scenario Outline: Login With Invalid Password
    Given I am in the login page of the Swag Labs Web Application
    When I enter "<username>" as username and "<password>" as password
    And I click the login button
    Then I should see an error message

    Examples:
      | username | password |
      | username | password |

  Scenario Outline: Login With Locked Out User
    Given I am in the login page of the Swag Labs Web Application
    When I enter "<username>" as username and "<password>" as password
    And I click the login button
    Then I should see an error message

    Examples:
      | username        | password     |
      | locked_out_user | secret_sauce |

  Scenario Outline: Login Successful
    Given I am in the login page of the Swag Labs Web Application
    When I enter "<username>" as username and "<password>" as password
    And I click the login button
    Then I should be taken to the product page

    Examples: 
      | username                | password     |
      | standard_user           | secret_sauce |
#      | problem_user            | secret_sauce |
#      | performance_glitch_user | secret_sauce |
