Feature: Product Functionality
  As a logged in user
  I should be able to sort items from A-Z

  Background: 
    Given I have logged in to the application

  Scenario Outline: Verity Sort order of products
    Given I am in the product page of the Swag Labs Web Application
    And the products are sorted from A-Z
    And I select Name(Z to A) from the sort filter
    Then the products should be filtered from Z to A
