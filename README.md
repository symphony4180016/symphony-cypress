# Symphony-qa

This project contains automated tests for the SauceDemo application using Cypress and Cucumber.

## Prerequisites

1. Node.js 16.x
2. Chrome browser installed on your local machine


## Running the tests locally

1. Clone the repository:
2. Navigate to the project directory:
3. Running cypress test.
```aidl
npm install
npx cypress run --browser chrome or npm run cypress
```

## Running the tests on GitLab CI/CD

1. Push your changes to GitLab:
```aidl
git add .
git commit -m "Your commit message"
git push origin main
```

2. The tests will be automatically executed on GitLab CI/CD using the configuration provided in the .gitlab-ci.yml file.

3. To view the test results, go to your project's GitLab page, click on "CI/CD" in the left sidebar, then click on the "Pipelines" tab. Click on the pipeline you want to view, and then click on the appropriate job (e.g., "java_test" or "cypress_test").=
4. You can view the result of the cypress tests 
5. You can view the test results on Gitlab by clicking on the "Pipeline" status button in the "Pipeline" page. And Then Clicking on Tests in the next page.

[Cypress Demo Login](cypress/videos/login.feature.mp4)
[Cypress Demo Product](cypress/videos/product.feature.mp4)






